package com.ovante.fggn.Perfection42;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class PrefectionCoinMarketcapApplication {


	public static void main(String[] args) {
		
		
		SpringApplication.run(PrefectionCoinMarketcapApplication.class, args);
	}

}
