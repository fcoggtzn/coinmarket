package com.ovante.fggn.Perfection42;

//import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import com.ovante.fggn.Perfection42.service.CoinMarketCapService;

@SpringBootTest
class PrefectionCoinMarketcapApplicationTests {

    @Autowired
	CoinMarketCapService servicio;
	
	@Test
	void conversionRate() {
	/* just to know how the service is working 
		servicio = new CoinMarketCapService();
	    RestTemplate plantilla = new RestTemplate();
	     //https://web-api.coinmarketcap.com/v1/tools/price-conversion?amount=1&convert_id=2790&id=2781
        
        // this is for testing the api key  that it's working by other hand not pass 
        HttpHeaders headers = servicio.createHttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
      
        ResponseEntity<String> response = plantilla.exchange(servicio.getUrl()+"/v1/tools/price-conversion?amount=1&convert=ETH&id=1", HttpMethod.GET, entity, String.class);
        System.out.println(response);
        assertEquals("ok","ok");
 */
		//https://web-api.coinmarketcap.com/v1/tools/price-conversion?amount=1&convert_id=2790&id=2781 to test from EUR to USD
		Long cointo = new Long(2781);
		Long coinfrom = new Long(2790);
		double rate= servicio.getRate(coinfrom, cointo);
		assertNotEquals("-1", rate);
	}

}
