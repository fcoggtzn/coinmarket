package com.ovante.fggn.Perfection42.model.coinmarket;

public class TestingModel {
	private String start ;
	private String limit;
	private String convert;
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	public String getConvert() {
		return convert;
	}
	public void setConvert(String convert) {
		this.convert = convert;
	}

}
