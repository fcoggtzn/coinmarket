package com.ovante.fggn.Perfection42.model.coinmarket;

public class ResponseApiCoin {
	private Data data;
	private Status status;
	
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	

}
