package com.ovante.fggn.Perfection42.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ovante.fggn.Perfection42.domain.Balance;
import com.ovante.fggn.Perfection42.domain.Operation;
import com.ovante.fggn.Perfection42.model.RespuestaApi;

@Service
public class AmountService {
	@Autowired
	CoinMarketCapService coinService;  // to get rate from the api
	@Autowired
	BalanceService balanceService;    // to manage the balance
	@Autowired
	OperationService operationService; // to audit or log operations
	
	double fee = 0.01; //fee will be 1% //it can be change by another source as database or environment for testing will use as is
	
	/*  * exchange some amount from, `amount` is in `from` currency, e.g.:
		    * request: from: USD, to: EUR, amount: 10
		    * response: rate: 0.9, amount: 9 EUR, fee: 0.1 USD, total: 10.1 USD*/
	public RespuestaApi amountFrom( String from,  String to, double amount ){
		RespuestaApi resp = new RespuestaApi();
		Balance balancefrom ;
		balancefrom = balanceService.getBalanceItem(from);
		Balance balanceTo ;
		balanceTo = balanceService.getBalanceItem(to);
		//for testing propose here you can check constrains 
		double rate = this.coinService.getRate(balancefrom.getId(),balanceTo.getId());
		double total =  amount*(1+fee);
		double amountTo = amount*rate;
		//we dont have requirement to trunk currency
		resp.setRate(rate);
		resp.setFee(amount*fee + " "+from);
		resp.setTotal(total+" "+from);
		resp.setAmount(amountTo+ " "+to );
		
		//actualizar base de datos
		Operation operation = new Operation();
		operation.setId(new Long(0));
		operation.setCreated_date(new Date());
		//its only for testing show up it can be more detail  it could be method  ---time up---
		operation.setOperation( "From  From: "+from+" --> "+to+" amount: "+ amount +"  rate: "+rate+ ", fee: "+resp.getFee()+", total:"+total+", converted :"+amountTo);
		this.operationService.saveOrUpdate(operation);
		
		
		return resp;
	}
	
	/* * exchange some amount to, like buying something, `amount` is in `to` currency, e.g.:
    * request: from: USD, to: EUR, amount: 10
    * response: rate: 0.9, amount: 10 EUR, fee: 0.11 USD, total: 11.22 USD
    * */	
	
	
	public RespuestaApi amountTo( String from,  String to, double amount ){
		RespuestaApi resp = new RespuestaApi();
		Balance balancefrom ;
		balancefrom = balanceService.getBalanceItem(from);
		Balance balanceTo ;
		balanceTo = balanceService.getBalanceItem(to);
		//for testing propose here you can check constrains 
		double rate = this.coinService.getRate(balancefrom.getId(),balanceTo.getId());
		double rateinv= 1/rate; //sacar la inversa
		double total = amount * rateinv;
		double feeto = total * fee;
		//Adjust the total + fee 
		//we dont have requirement to trunk currency
		total += feeto;
		resp.setRate(rate);
		resp.setFee(feeto + " "+from);
		resp.setTotal(total+" "+from);
		resp.setAmount(amount+ " "+to );
		
		//actualizar base de datos
				Operation operation = new Operation();
				operation.setId(new Long(0));
				operation.setCreated_date(new Date());
				//its only for testing show up it can be more detail  it could be method  ---time up---
				operation.setOperation( "TO  From: "+from+" --> "+to+" amount: "+ amount +"  rate: "+rate+ ", fee: "+resp.getFee()+", total:"+total+", converted :"+amount);
				this.operationService.saveOrUpdate(operation);
				
				
				// it can be the next stage its not a request 
				//balancefrom.setBalance(balancefrom.getBalance() - total);
				//save to database
				
		
		return resp;
	}
	
	
	/*
	 *  * exchange all, take all balance, e.g.: balance is 10 USD
    * request: from: USD, to: EUR
    * response: rate: 0.9, amount: 8.91 EUR, fee: 0.1 USD, total: 10 USD
	 */	
	public RespuestaApi amountAll( String from,  String to, double amount ){
		RespuestaApi resp = new RespuestaApi();
		Balance balancefrom ;
		balancefrom = balanceService.getBalanceItem(from);
		Balance balanceTo ;
		balanceTo = balanceService.getBalanceItem(to);

		double rate = this.coinService.getRate(balancefrom.getId(),balanceTo.getId());
		double feeto = amount * fee;
		double total = (amount - feeto) * rate;
		//we dont have requirement to trunk currency
		resp.setRate(rate);
		resp.setFee(feeto + " "+from);
		resp.setTotal(amount+" "+from);
		resp.setAmount(total+ " "+to );
		
		//actualizar base de datos
				Operation operation = new Operation();
				operation.setId(new Long(0));
				operation.setCreated_date(new Date());
				//its only for testing show up it can be more detail  it could be method  ---time up---
				operation.setOperation( "All  From: "+from+" --> "+to+" amount: "+ amount +"  rate: "+rate+ ", fee: "+resp.getFee()+", total:"+amount+", converted :"+total);
				this.operationService.saveOrUpdate(operation);
				
				
		
		
		return resp;
	}
	
}
