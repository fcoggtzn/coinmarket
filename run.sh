#!/bin/bash 

#docker stop coinmarket
#docker rm coinmarket
 
docker run --name coinmarket  -e TZ=America/Mexico_City  -p8080:8080 -d fggn/coinmarket
#copy h2 database
docker cp coinmarketDB.mv.db coinmarket:/
docker cp coinmarketDB.trace.db coinmarket:/
#init database
sleep 60
curl http://localhost:8080/v1/init
