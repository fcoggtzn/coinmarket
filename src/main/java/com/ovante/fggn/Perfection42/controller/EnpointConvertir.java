package com.ovante.fggn.Perfection42.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ovante.fggn.Perfection42.domain.Balance;
import com.ovante.fggn.Perfection42.domain.Operation;
import com.ovante.fggn.Perfection42.model.RespuestaApi;
import com.ovante.fggn.Perfection42.service.AmountService;
import com.ovante.fggn.Perfection42.service.BalanceService;
import com.ovante.fggn.Perfection42.service.OperationService;

@RestController
@RequestMapping("v1")
public class EnpointConvertir {
	
	@Autowired
	BalanceService balanceService;
	
	@Autowired
	AmountService amountServ;
	
	@Autowired
	OperationService oprService;
	
	/* * exchange some amount from, `amount` is in `from` currency, e.g.:
    * request: from: USD, to: EUR, amount: 10
    * response: rate: 0.9, amount: 9 EUR, fee: 0.1 USD, total: 10.1 USD
    * */
	@GetMapping("/convert/from")
	public RespuestaApi amountFrom(@RequestParam String from, @RequestParam String to,@RequestParam double amount ,final HttpServletResponse response) {
		//cache for 1 minute == 60 Sec
	    response.addHeader("Cache-Control", "max-age=60, must-revalidate, no-transform");
		return amountServ.amountFrom(from, to, amount);
	}
	
	/* * exchange some amount to, like buying something, `amount` is in `to` currency, e.g.:
    * request: from: USD, to: EUR, amount: 10
    * response: rate: 0.9, amount: 10 EUR, fee: 0.11 USD, total: 11.22 USD
    * */	
	@GetMapping("/convert/to")
	public RespuestaApi amountTo(@RequestParam String from, @RequestParam String to,@RequestParam double amount ,final HttpServletResponse response) {
		//cache for 1 minute == 60 Sec
	    response.addHeader("Cache-Control", "max-age=60, must-revalidate, no-transform");
		return amountServ.amountTo(from, to, amount);
	}
	
	
	/*
	 *  * exchange all, take all balance, e.g.: balance is 10 USD
    * request: from: USD, to: EUR
    * response: rate: 0.9, amount: 8.91 EUR, fee: 0.1 USD, total: 10 USD
	 */
	@GetMapping("/convert/all")
	public RespuestaApi amountAll(@RequestParam String from, @RequestParam String to,@RequestParam double amount ,final HttpServletResponse response) {
		//cache for 1 minute == 60 Sec
	    response.addHeader("Cache-Control", "max-age=60, must-revalidate, no-transform");
		return amountServ.amountAll(from, to, amount);
	}
	
	/*Aditional enpoints */
	

	/*
	 *  * `/init` - to initialize db with BTC, ETH, VNDC, USD, EUR currencies and 1000 balance for every currency, flush operations and cache.
	 */
	@GetMapping("/init")
	public String init() {
	
		return balanceService.init();
	}
	
	/*
	 *  * `/balances` - list all currencies and balances
	 */
	@GetMapping("/balances")
	public List<Balance> balances(final HttpServletResponse response) {
		//cache for 1 minute == 60 Sec
	    response.addHeader("Cache-Control", "max-age=60, must-revalidate, no-transform");
		return balanceService.getBalance();
	}
	
	
	/*
	 * /operations` - ordered list of all operations
	 */
	@GetMapping("/operations")
	public List<Operation> operations(final HttpServletResponse response) {
		//cache for 1 minute == 60 Sec
	    response.addHeader("Cache-Control", "max-age=60, must-revalidate, no-transform");
	    return this.oprService.findAll();
		
	}
	
	
	
}
