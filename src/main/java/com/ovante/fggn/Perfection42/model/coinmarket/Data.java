package com.ovante.fggn.Perfection42.model.coinmarket;

public class Data {
	
		  private float id;
		  private String symbol;
		  private String name;
		  private float amount;
		  private String last_updated;
		  private Quote quote;


		 // Getter Methods 

		  public float getId() {
		    return id;
		  }

		  public String getSymbol() {
		    return symbol;
		  }

		  public String getName() {
		    return name;
		  }

		  public float getAmount() {
		    return amount;
		  }

		  public String getLast_updated() {
		    return last_updated;
		  }

		  public Quote getQuote() {
		    return quote;
		  }

		 // Setter Methods 

		  public void setId( float id ) {
		    this.id = id;
		  }

		  public void setSymbol( String symbol ) {
		    this.symbol = symbol;
		  }

		  public void setName( String name ) {
		    this.name = name;
		  }

		  public void setAmount( float amount ) {
		    this.amount = amount;
		  }

		  public void setLast_updated( String last_updated ) {
		    this.last_updated = last_updated;
		  }

		  public void setQuote( Quote quoteObject ) {
		    this.quote = quoteObject;
		  }
		}