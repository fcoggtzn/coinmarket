package com.ovante.fggn.Perfection42;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ovante.fggn.Perfection42.model.RespuestaApi;
import com.ovante.fggn.Perfection42.service.AmountService;
import com.ovante.fggn.Perfection42.service.BalanceService;
@SpringBootTest(classes= {PrefectionCoinMarketcapApplication.class, BalanceService.class })
class AmountTesterService {

	@Autowired
	BalanceService balanceService;
	@Autowired
	AmountService amountService;

	@BeforeEach
	void setUp() throws Exception {
		//first init database
				balanceService.init();				
	}

	@Test
	void test() {
		//constrains  as example that was on requirements
		/*
		 *   * exchange some amount from, `amount` is in `from` currency, e.g.:
    * request: from: USD, to: EUR, amount: 10
    * response: rate: 0.9, amount: 9 EUR, fee: 0.1 USD, total: 10.1 USD
		 */
		 RespuestaApi resp = amountService.amountFrom("USD","EUR",10);
		 
		 assertNotNull(resp);
		 assertNotNull(resp.getAmount());
		 assertNotNull("0.1 USD",resp.getFee());
	}
	
	@Test
	void test2() {
		//constrains  as example that was on requirements
		/*
		 * exchange some amount to, like buying something, `amount` is in `to` currency, e.g.:
    * request: from: USD, to: EUR, amount: 10
    * response: rate: 0.9, amount: 10 EUR, fee: 0.11 USD, total: 11.22 USD
		 */
		 RespuestaApi resp = amountService.amountTo("USD","EUR",10);
		 
		 assertNotNull(resp);
		 assertNotNull(resp.getAmount());
		
	}
	
	@Test
	void test3() {
		//constrains  as example that was on requirements
		/* exchange all, take all balance, e.g.: balance is 10 USD
		    * request: from: USD, to: EUR
		    * response: rate: 0.9, amount: 8.91 EUR, fee: 0.1 USD, total: 10 USD
		    */
		 RespuestaApi resp = amountService.amountAll("USD","EUR",10);
		 
		 assertNotNull(resp);
		 assertNotNull(resp.getAmount());
	
		
	}
	
	@Test
	void ErrorTest() {
		try{
		// when send something wrong send nullpointer
		 amountService.amountAll("SD","EUR",10);	
			fail("Not Error when you send bad currency");

		}
		catch (Exception e) {
		   System.out.println("Excelent");
		}
	}
	
}
