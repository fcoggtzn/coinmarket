package com.ovante.fggn.Perfection42.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import com.ovante.fggn.Perfection42.domain.Balance;
import com.ovante.fggn.Perfection42.repository.BalanceRepository;
import com.ovante.fggn.Perfection42.repository.OperationRepository;

@Service
public class BalanceService {
	 @Autowired
	 private BalanceRepository balanceRepository;
	 
	 @Autowired
	 private OperationRepository operationRepository;
	 
	 /*
	 `/init` - to initialize db with BTC, ETH, VNDC, USD, EUR currencies and 1000 balance for every currency, flush operations and cache.
	 */
	 public String init() {
		 // // BTC 1, ETH 1027, VNDC 4805, USD 2781 , EUR 2790
		// Balance balance = new Balance("BTC",1000);
		 Balance balance = new Balance(new Long(1),"BTC",1000.00);
		 this.balanceRepository.save(balance);
		  balance = new Balance(new Long(1027),"ETH",1000.00);
		  this.balanceRepository.save(balance);
		  balance = new Balance(new Long(4805),"VNDC",1000.00);
		  this.balanceRepository.save(balance);
		  balance = new Balance(new Long(2781),"USD",1000.00);
  		  this.balanceRepository.save(balance);
  		  balance = new Balance(new Long(2790),"EUR",1000.00);
		  this.balanceRepository.save(balance);		 
		  this.operationRepository.deleteAll();
		  //cache will be automatic using cachemanager 
		  
		return "OK";
		 
	 }
	 
	 
	 public List<Balance> getBalance(){
		 return this.balanceRepository.findAll();
	 }
	 
	 public Balance getBalanceItem(String currency) {
		return this.balanceRepository.findByCurrency(currency);
	 }
	 

}
