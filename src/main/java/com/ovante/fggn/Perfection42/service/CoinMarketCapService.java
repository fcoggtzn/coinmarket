package com.ovante.fggn.Perfection42.service;


import java.util.concurrent.TimeUnit;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service
public class CoinMarketCapService {
	//private String apikey="4a03a564-0d51-4001-ac0d-3cd083630002"; 
	private String apikey="c24b046f-5c71-48ad-865c-3af1108e5578";
	private String headerName ="X-CMC_PRO_API_KEY";
 	private String url="https://sandbox-api.coinmarketcap.com";
	//private String url="https://pro-api.coinmarketcap.com";
	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	public String getHeaderName() {
		return headerName;
	}
	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	private HttpHeaders createHttpHeaders()
	{
	   
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    headers.add(headerName, apikey);
	    // to keep 1 minute cache 
	    headers.setCacheControl(CacheControl.maxAge(1, TimeUnit.MINUTES)
                .cachePrivate()
                .mustRevalidate());
	    return headers;
	}
	
// for the  chache require	
	@Cacheable("coinmarket")
	public double getRate(Long coinfrom,Long cointo)  {
		double rate=-1;
	    RestTemplate plantilla = new RestTemplate();
		
		 // this is for testing the api key  that it's working by other hand not pass 
	    //		//https://web-api.coinmarketcap.com/v1/tools/price-conversion?amount=1&convert_id=2790&id=2781 to test from EUR to USD
	    // https://pro-api.coinmarketcap.com//v1/tools/price-conversion?amount=1&convert=VNDC&id=2781

        HttpHeaders headers = createHttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
      
        ResponseEntity<String> response = plantilla.exchange(this.url+"/v1/tools/price-conversion?amount=1&convert_id="+cointo+"&id="+coinfrom, HttpMethod.GET, entity, String.class);
      // System.out.println(response.getBody().getData().getQuote());
        try {  //tray to lookfor the information about it otherwise rate -1 and its error
             ObjectMapper mapper = new ObjectMapper();
             JsonNode root = null;
        	root = mapper.readTree(response.getBody());
			 JsonNode data = root.path("data");
		      rate = new Double(data.get("quote").get(cointo+"").get("price").toString());
		        
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
             
        
       return rate;
		
	}

	
		
	
	

}
