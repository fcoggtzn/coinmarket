package com.ovante.fggn.Perfection42.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ovante.fggn.Perfection42.domain.Operation;

public interface OperationRepository extends CrudRepository<Operation, Long> {
	@Override
    List<Operation> findAll();

}
