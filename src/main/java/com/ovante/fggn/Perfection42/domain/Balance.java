package com.ovante.fggn.Perfection42.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Balance {
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String currency;
	private double balance;
	private Date created_date;
	private Date updated_date;
	
	public Balance() {
		this.created_date = new Date();
		this.updated_date = new Date();
	}
	
	public Balance(Long id, String currency, double balance) {
		super();		
		this.id = id;
		this.currency = currency;
		this.balance = balance;
		this.created_date = new Date();
		this.updated_date = new Date();
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	
}
