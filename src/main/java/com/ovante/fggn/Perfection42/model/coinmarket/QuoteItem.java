package com.ovante.fggn.Perfection42.model.coinmarket;


public class QuoteItem {
	  private double price;
	  private String last_updated;


	 // Getter Methods 

	  public double getPrice() {
	    return price;
	  }

	  public String getLast_updated() {
	    return last_updated;
	  }

	 // Setter Methods 

	  public void setPrice( double price ) {
	    this.price = price;
	  }

	  public void setLast_updated( String last_updated ) {
	    this.last_updated = last_updated;
	  }
	}
