package com.ovante.fggn.Perfection42.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Operation {
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;
   private String operation;
   private Date created_date;
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getOperation() {
	return operation;
}
public void setOperation(String operation) {
	this.operation = operation;
}
public Date getCreated_date() {
	return created_date;
}
public void setCreated_date(Date created_date) {
	this.created_date = created_date;
}
   
}
