package com.ovante.fggn.Perfection42.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ovante.fggn.Perfection42.domain.Operation;
import com.ovante.fggn.Perfection42.repository.OperationRepository;

@Service
public class OperationService {
	
	@Autowired 
	OperationRepository operationRepository;
	
	public void saveOrUpdate(Operation operation) {
		this.operationRepository.save(operation);
	}
	
	public List<Operation> findAll(){
		return this.operationRepository.findAll();
	}

}
