package com.ovante.fggn.Perfection42.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ovante.fggn.Perfection42.domain.Balance;

public interface BalanceRepository extends CrudRepository<Balance, Long> {
	
	@Override
    List<Balance> findAll();
	
	Balance findByCurrency(String currency);

}
