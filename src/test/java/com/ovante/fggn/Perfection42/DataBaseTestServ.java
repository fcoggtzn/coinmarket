package com.ovante.fggn.Perfection42;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
/*to test de data base an init porgram */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ovante.fggn.Perfection42.domain.Balance;
import com.ovante.fggn.Perfection42.service.BalanceService;

@SpringBootTest(classes= {PrefectionCoinMarketcapApplication.class, BalanceService.class })
class DataBaseTestServ {
	
	@Autowired
	BalanceService balanceService;

	@BeforeEach
	void setUp() throws Exception {
		//first init database
				balanceService.init();				
	}

	@Test
	void test() {
		
		// check the database
	 Balance item=	balanceService.getBalanceItem("USD");
	 assertEquals("USD", item.getCurrency());
	 assertEquals(1000.0, item.getBalance());
	 
	}

}
